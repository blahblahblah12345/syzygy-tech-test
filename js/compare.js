
angular.module('compare', ['ngTouch'])
    
    .factory('getProducts', ['$timeout', function($timeout) {

        // TODO: link to real db
        // would usually make a call to db for product data
        // in this case it's hardcoded:
        var products = dbResponse;

        return function(callback) {

            // fake async with $timeout
            $timeout(function() {
                callback(null, products);
            }, 0);

        };

    }])

    .controller('MainCtrl', ['getProducts', function(getProducts) {

        var main = this;
        
        main.products = [];
        main.selected = [null, null, null];

        // will filter out array items in comparisonArr
        // used to prevent selecting the same product twice
        main.notInArray = function(comparisonArr) {

            return function(item) {

                return (comparisonArr.indexOf(item) === -1);

            };

        };

        // get product list from db
        getProducts(function(err, data) {

            if (err) {
                // TODO: handle error
                // which won't happen in this mock-up so just ...
                return;
            }

            main.products = data;

        });

    }]);